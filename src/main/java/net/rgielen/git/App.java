package net.rgielen.git;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        sayHelloGit(args);
    }

    private static void sayHelloGit(String[] args) {
        String name = "Git";
        if (args != null && args.length>0) {
            name = args[0];
        }
        System.out.println("Hallo " + name + ".");
    }

}
